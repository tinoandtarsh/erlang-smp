-module(smp_sup).
-behaviour(supervisor).

%% External exports
-export([
  start_link/0
]).

%% supervisor callbacks
-export([init/1]).

-include("common.hrl").

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	smp_db:start(),
	Web = {
		webmachine_mochiweb, {
			webmachine_mochiweb, start, [smp_config:web_config()]
		},
		permanent, 5000, worker, [mochiweb_socket_server]
	},
    Processes = [Web],
    {ok, { {one_for_one, 10, 10}, Processes} }.
