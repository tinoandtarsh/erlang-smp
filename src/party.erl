%%-*- mode: erlang -*-
%%%-------------------------------------------------------------------
%%% @author martijn
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%% Service Group Resource
%%%
%%% @end
%%% Created : 08. May 2016 8:27 PM
%%%-------------------------------------------------------------------
%%%
-module(party).
-author('martijn.vandenboogaard@gmail.com').
-vsn(0.1).

-export([ split/1
        , join/2
        ]).

-define(ID_SEP_PAT, "\:\:").

split(Key) -> re:split(Key, ?ID_SEP_PAT, [{return, list}]).

join(Scheme, Id) -> Scheme ++ "::" ++ Id.

