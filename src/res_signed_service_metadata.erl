-module(res_signed_service_metadata).
-export([
	allowed_methods/2,
	content_types_accepted/2,
	content_types_provided/2,
    init/1,
    to_xml/2
]).

-include_lib("webmachine/include/webmachine.hrl").

init([]) ->
    {ok, undefined}.

allowed_methods(ReqData, State) ->
	{['DELETE', 'GET', 'POST', 'PUT', 'HEAD'], ReqData, State}.

% Get resource
%
content_types_provided(ReqData, State) ->
	{[{"application/xml", to_xml}], ReqData, State}.

to_xml(ReqData, State) ->
	{ParticipantId, DocumentId} = {wrq:path_info(participant_id, ReqData), wrq:path_info(document_id, ReqData)},
	{[ParticipantId, DocumentId], ReqData, State}.

% Create or update resource
%
content_types_accepted(ReqData, State) ->
    { [ {"application/xml", from_xml} ], ReqData, State }.

