%% id = scheme::bid
%% scheme = e.g. iso6523:0151
%% bid = business id
-record(smp_participant, { key
                         , scheme
                         , id }).
%%
-record(service_information, { participant_key :: string()
                             , document_key}).

-record(process, { key :: string()
                 , scheme :: string()
                 , id :: string()
                 , participant_key :: string()
                 , document_key :: string()}).
%%-record(smp_capability, 
