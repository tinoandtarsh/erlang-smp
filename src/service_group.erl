%%-*- mode: erlang -*-
%%%-------------------------------------------------------------------
%%% @author martijn
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. May 2016 8:27 PM
%%%-------------------------------------------------------------------
-module(service_group).
-author('martijn.vandenboogaard@gmail.com').
-vsn(0.1).
-export([
	can_create/1,
	delete/1,
	get/1,
	post/1,
	put/1,
	is_valid_id/1
]).

-define(ID_SEP_PAT, "\:\:").

-include("common.hrl").
-include("ddl.hrl").

split_id(Key) -> re:split(Key, ?ID_SEP_PAT, [{return, list}]).

% Fetch a service group element based on the given Id
% Return the XML document
get(Key) ->
  Rec = smp_db:get_participant(Key),
	case Rec of
    {atomic, [Row]} ->
      Data = { 'ServiceGroup'
             , [{scheme, Row#smp_participant.scheme}]
             , [Row#smp_participant.id]};
    {atomic, []} -> Data = ""
  end,
  ?LOG_INFO(Data),
	lists:flatten(xmerl:export_simple([Data], xmerl_xml)).

post(Key) ->
	[Scheme, Id] = split_id(Key),
	smp_db:create_participant(Key, Id, Scheme),
	[Key].

put(Data) ->
	?LOG_INFO("storing " ++ Data),
	[Data].

delete(Id) ->
	?LOG_INFO("removing " ++ Id),
	true.

can_create(Key) -> not smp_db:has_participant(Key).

%% Check if the id is a valid id
is_valid_id(MaybeId) ->
	case re:run(MaybeId, ?ID_SEP_PAT) of
		nomatch -> false;
    {match, _} -> true
	end.

%json_body(QS) -> mochijson:encode({struct, QS}).
