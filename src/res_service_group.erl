%%-*- mode: erlang -*-
%%%-------------------------------------------------------------------
%%% @author martijn
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%% Service Group Resource
%%%
%%% @end
%%% Created : 08. May 2016 8:27 PM
%%%-------------------------------------------------------------------
%%%
-module(res_service_group).
-author('martijn.vandenboogaard@gmail.com').
-vsn(0.1).

-export(
  [ allowed_methods/2
  , content_types_accepted/2
  , content_types_provided/2
  , create_path/2
  , delete_resource/2
  , from_xml/2
  , init/1
  , is_authorized/2
  , post_is_create/2
  , process_post/2
  , malformed_request/2
  , to_xml/2
  ]).

-include_lib("common.hrl").
-include_lib("webmachine/include/webmachine.hrl").
-include_lib("party.hrl").

init([]) -> {ok, {node, node()}}.

allowed_methods(ReqData, State) ->
  { [ 'DELETE'
    , 'GET'
    , 'HEAD'
    , 'POST'
    , 'PUT'
    ], ReqData, State}.

%%
%% Only allow safe methods unauthorised
%% Unsafe methods must be authorised
%% 
is_authorized(ReqData, State) ->
  case wrq:method(ReqData) of
    'GET' -> {true, ReqData, State};
    'HEAD' -> {true, ReqData, "Success"};
    _ -> {"Basic realm=DBC", ReqData, State}
  end.

%%
%% Party has format of scheme::id
%% Returns false if the request is ok, true if the request is malformed
%%
malformed_request(ReqData, State) ->
  Key = http_uri:decode(wrq:path_info(participant_id, ReqData)),
  case party:split(Key) of
    [Scheme, Id] -> {false, ReqData, #party{key=Key, scheme=Scheme, id=Id}};
    _ -> {true, ReqData, State}
  end.

%%
%% Get resource
%%
content_types_provided(ReqData, State) ->
  { [{"text/xml", to_xml}], ReqData, State }.

to_xml(ReqData, State) ->
  ?LOG_INFO("DEBUG"),
  ?LOG_INFO(State#party.key),
  { service_group:get(State#party.key)
  , ReqData, State
  }.

%%
%% Create or update resource
%%
content_types_accepted(ReqData, State) ->
    { [ {"application/xml", from_xml},
        {"text/xml", from_xml} 
      ], ReqData, State }.

post_is_create(ReqData, State) ->
  { service_group:can_create(State#party.key)
  , ReqData, State }.

create_path(ReqData, State) -> {"", ReqData, State}.

process_post(ReqData, State) ->
  case service_group:can_create(State#party.key) of
    false -> 
      { {halt, 200}, wrq:set_resp_body("Already exists", ReqData), State};
    _ -> {true, ReqData, State}
  end.

from_xml(ReqData, State) ->
  case wrq:method(ReqData) of
    'POST' -> service_group:post(State#party.key);
    'PUT' -> service_group:put("putting")
  end,
  {true, wrq:set_resp_body("created", ReqData), State}.

%%
%% Delete resource
%%
delete_resource(ReqData, State) ->
  ParticipantId = State#party.key,
  { service_group:delete(ParticipantId), ReqData, State }.

