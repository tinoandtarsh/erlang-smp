%%-*- mode: erlang -*-
%%%-------------------------------------------------------------------
%%% @author martijn
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. May 2016 8:27 PM
%%%-------------------------------------------------------------------
-module(smp_db).
-author("martijn").

%% API
-export(
  [ start/0
  , create_participant/3
  , has_participant/1
  , get_participant/1
  ]).

-include("common.hrl").
-include("ddl.hrl").

start() ->
  application:stop(mnesia),
  mnesia:create_schema([node()]),
  application:start(mnesia),
  Fields = record_info(fields, smp_participant),
  ?LOG_INFO(mnesia:create_table(smp_participant, [
    {disc_copies, [node()]},
    {ram_copies, nodes()},
    {type, set},
    {attributes, Fields}
  ])).

create_participant(Key, Id, Scheme) ->
  Rec = #smp_participant{key = Key, id = Id, scheme = Scheme},
  ?LOG_INFO(mnesia:transaction(fun() -> mnesia:write(Rec) end)).

get_participant(Key) ->
  ?LOG_INFO(Key),
  mnesia:transaction(fun() -> mnesia:read({smp_participant, Key}) end).

has_participant(Key) ->
  case mnesia:dirty_read(smp_participant, Key) of
    [] -> false;
    _ -> true
  end.
