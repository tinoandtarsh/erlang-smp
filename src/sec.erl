%%-*- mode: erlang -*-
%%%-------------------------------------------------------------------
%%% @author martijn
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%% Security module
%%% @end
%%% Created : 08. May 2016 8:27 PM
%%%-------------------------------------------------------------------
-module(sec).
-author('martijn.vandenboogaard@gmail.com').
-vsn(0.1).
-export([
  auth/1
]).

auth(Token) -> true.
