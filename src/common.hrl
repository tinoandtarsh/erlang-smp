
-ifndef(PRINT).
-define(PRINT(Var), io:format("DEBUG: ~p:~p - ~p~n~n ~p~n~n", [?MODULE, ?LINE, ??Var, Var])).
-endif.

-ifndef(LOG_INFO).
-define(LOG_INFO(Var), error_logger:info_msg(io_lib:format("DEBUG: ~p:~p - ~p~n~n ~p~n~n", [?MODULE, ?LINE, ??Var, Var]))).
-endif.

%
% <?xml version="1.0" encoding="UTF-8"?>
% <smp:ServiceGroup xmlns:smp="http://docs.oasis-open.org/bdxr/ns/SMP/2014/07" xmlns:dbc="http://smp.digitalbusinesscouncil.com.au">
%   <smp:ParticipantIdentifier scheme="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">23601120601</smp:ParticipantIdentifier>
%   <smp:ServiceMetadataReferenceCollection>
%     <!--Zero or more repetitions:-->
%     <smp:ServiceMetadataReference href="http://103.1.205.55:8080/s0151%3a23601120601/services/bdx-docid-qns%3a%3aurn%3aoasis%3anames%3aspecification%3aubl%3aschema%3axsd%3aInvoice-2%3a%3aInvoice%23%23UBL-2.0"/>
%   </smp:ServiceMetadataReferenceCollection>
%   <!--Optional:-->
%   <smp:Extension>
%     <!--You may enter ANY elements at this point-->
%       <dbc:OtherIdentifiers>
%         <smp:ParticipantIdentifier scheme="GLN">unique nr</smp:ParticipantIdentifier>
%         <smp:ParticipantIdentifier scheme="DUNS">unique nr</smp:ParticipantIdentifier>
%       </dbc:OtherIdentifiers>
%     <AnyElement/>
%   </smp:Extension>
% </smp:ServiceGroup>
% 
% <?xml version="1.0" encoding="utf-8" ?>
% <ns:SignedServiceMetadata xmlns:ns="http://docs.oasis-open.org/bdxr/ns/SMP/2014/07" xmlns:xd="http://www.w3.org/2000/09/xmldsig#">
%   <ns:ServiceMetadata>
%     <!--You have a CHOICE of the next 2 items at this level-->
%     <ns:ServiceInformation>
%       <ns:ParticipantIdentifier scheme="urn:oasis:names:tc:ebcore:partyid-type:iso6523">0151:23601120601</ns:ParticipantIdentifier>
%       <ns:DocumentIdentifier scheme="bdx-docid-qns">urn:oasis:names:specification:ubl:schema:xsd:Invoice-2</ns:DocumentIdentifier>
%       <ns:ProcessList>
%         <!--1 or more repetitions:-->
%         <ns:Process>
%           <ns:ProcessIdentifier scheme="string">Invoicing</ns:ProcessIdentifier>
%           <ns:ServiceEndpointList>
%             <!--1 or more repetitions:-->
%             <ns:Endpoint transportProfile="string">
%               <ns:EndpointURI>http://www.company.org/cum/sonoras</ns:EndpointURI>
%               <ns:RequireBusinessLevelSignature>true</ns:RequireBusinessLevelSignature>
%               <!--Optional:-->
%               <ns:MinimumAuthenticationLevel>string</ns:MinimumAuthenticationLevel>
%               <!--Optional:-->
%               <ns:ServiceActivationDate>2014-06-10T01:15:04+10:00</ns:ServiceActivationDate>
%               <!--Optional:-->
%               <ns:ServiceExpirationDate>2008-11-16T03:52:58</ns:ServiceExpirationDate>
%               <ns:Certificate>Y2lyY3Vt</ns:Certificate>
%               <ns:ServiceDescription>string</ns:ServiceDescription>
%               <ns:TechnicalContactUrl>http://www.test.com/fremunt/foedere</ns:TechnicalContactUrl>
%               <!--Optional:-->
%               <ns:TechnicalInformationUrl>http://www.corp.gov/sceptra/et</ns:TechnicalInformationUrl>
%               <!--Optional:-->
%               <ns:Extension>
%                 <!--You may enter ANY elements at this point-->
%                 <AnyElement/>
%               </ns:Extension>
%             </ns:Endpoint>
%           </ns:ServiceEndpointList>
%           <!--Optional:-->
%           <ns:Extension>
%             <!--You may enter ANY elements at this point-->
%             <AnyElement/>
%           </ns:Extension>
%         </ns:Process>
%       </ns:ProcessList>
%       <!--Optional:-->
%       <ns:Extension>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%       </ns:Extension>
%     </ns:ServiceInformation>
%     <ns:Redirect href="http://www.my.edu/flammato/speluncis">
%       <ns:CertificateUID>string</ns:CertificateUID>
%       <!--Optional:-->
%       <ns:Extension>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%       </ns:Extension>
%     </ns:Redirect>
%   </ns:ServiceMetadata>
%   <xd:Signature Id="string">
%     <xd:SignedInfo Id="string">
%       <xd:CanonicalizationMethod Algorithm="http://www.company.edu/nubibus/flammas">
%         ac
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%         hoc
%       </xd:CanonicalizationMethod>
%       <xd:SignatureMethod Algorithm="http://www.test.org/caelumque/speluncis">
%         circum
%         <!--Optional:-->
%         <xd:HMACOutputLength>100</xd:HMACOutputLength>
%         aris
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%         coniunx
%       </xd:SignatureMethod>
%       <!--1 or more repetitions:-->
%       <xd:Reference Id="string" URI="http://www.any.com/claustra/circum" Type="http://www.any.com/quisquam/et">
%         <!--Optional:-->
%         <xd:Transforms>
%           <!--1 or more repetitions:-->
%           <xd:Transform Algorithm="http://www.sample.gov/ac/sed">
%             pectore
%             <!--You have a CHOICE of the next 2 items at this level-->
%             <!--You may enter ANY elements at this point-->
%             <AnyElement/>
%             <xd:XPath>string</xd:XPath>
%             talia
%           </xd:Transform>
%         </xd:Transforms>
%         <xd:DigestMethod Algorithm="http://www.corp.edu/annos/sciret">
%           certo
%           <!--You may enter ANY elements at this point-->
%           <AnyElement/>
%           dare
%         </xd:DigestMethod>
%         <xd:DigestValue>cmF0ZXM=</xd:DigestValue>
%       </xd:Reference>
%     </xd:SignedInfo>
%     <xd:SignatureValue Id="string">YmVsbGE=</xd:SignatureValue>
%     <!--Optional:-->
%     <xd:KeyInfo Id="string">
%       volutans
%       <!--You have a CHOICE of the next 8 items at this level-->
%       <xd:KeyName>string</xd:KeyName>
%       <xd:KeyValue>
%         dedit
%         <!--You have a CHOICE of the next 3 items at this level-->
%         <xd:DSAKeyValue>
%           <xd:P>ZmFjaWF0</xd:P>
%           <xd:Q>bWFnbm8=</xd:Q>
%           <!--Optional:-->
%           <xd:G>ZXQ=</xd:G>
%           <xd:Y>YWM=</xd:Y>
%           <!--Optional:-->
%           <xd:J>ZXQ=</xd:J>
%           <xd:Seed>cmVnZW1xdWU=</xd:Seed>
%           <xd:PgenCounter>YW5ub3M=</xd:PgenCounter>
%         </xd:DSAKeyValue>
%         <xd:RSAKeyValue>
%           <xd:Modulus>aW5maXhpdA==</xd:Modulus>
%           <xd:Exponent>dmVudG9z</xd:Exponent>
%         </xd:RSAKeyValue>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%         tenens
%       </xd:KeyValue>
%       <xd:RetrievalMethod URI="http://www.my.org/rapidum/habenas" Type="http://www.sample.org/cum/iovisque">
%         <!--Optional:-->
%         <xd:Transforms>
%           <!--1 or more repetitions:-->
%           <xd:Transform Algorithm="http://www.corp.com/abdidit/iunonis">
%             coniunx
%             <!--You have a CHOICE of the next 2 items at this level-->
%             <!--You may enter ANY elements at this point-->
%             <AnyElement/>
%             <xd:XPath>string</xd:XPath>
%             adorat
%           </xd:Transform>
%         </xd:Transforms>
%       </xd:RetrievalMethod>
%       <xd:X509Data>
%         <!--You have a CHOICE of the next 6 items at this level-->
%         <xd:X509IssuerSerial>
%           <xd:X509IssuerName>string</xd:X509IssuerName>
%           <xd:X509SerialNumber>100</xd:X509SerialNumber>
%         </xd:X509IssuerSerial>
%         <xd:X509SKI>aXBzYQ==</xd:X509SKI>
%         <xd:X509SubjectName>string</xd:X509SubjectName>
%         <xd:X509Certificate>aWxsdW0=</xd:X509Certificate>
%         <xd:X509CRL>ZnJlbmF0</xd:X509CRL>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%       </xd:X509Data>
%       <xd:PGPData>
%         <!--You have a CHOICE of the next 2 items at this level-->
%         <xd:PGPKeyID>YXJpcw==</xd:PGPKeyID>
%         <!--Optional:-->
%         <xd:PGPKeyPacket>bmltYm9ydW0=</xd:PGPKeyPacket>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%         <xd:PGPKeyPacket>YXJpcw==</xd:PGPKeyPacket>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%       </xd:PGPData>
%       <xd:SPKIData>
%         <xd:SPKISexp>bW9sbGl0cXVl</xd:SPKISexp>
%         <!--You may enter ANY elements at this point-->
%         <AnyElement/>
%       </xd:SPKIData>
%       <xd:MgmtData>string</xd:MgmtData>
%       <!--You may enter ANY elements at this point-->
%       <AnyElement/>
%       premere
%     </xd:KeyInfo>
%     <!--Zero or more repetitions:-->
%     <xd:Object Id="string" MimeType="string" Encoding="http://www.test.edu/molemque/ferant">
%       gero
%       <!--You may enter ANY elements at this point-->
%       <AnyElement/>
%       nimborum
%     </xd:Object>
%   </xd:Signature>
% </ns:SignedServiceMetadata>
% 
