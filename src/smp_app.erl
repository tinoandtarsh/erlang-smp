-module(smp_app).

-behaviour(application).
-export([
    start/2,
    stop/1
]).

-include("common.hrl").
-include("ddl.hrl").

start(_Type, _StartArgs) ->
  smp_sup:start_link().

stop(_State) ->
    ok.
